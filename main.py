import os, glob, time
import pandas as pd
import cx_Oracle
import datetime as dt
import http.client
import mimetypes
from codecs import encode

def searchFileName():
    search_dir = r"data/"
    files = filter(os.path.isfile, glob.glob(search_dir + "/*.csv"))
    file_date_tuple_list = [x for x in files]
    file_date_tuple_list.sort(key=lambda x: x)
    return file_date_tuple_list

def readCSVtoDataFrame(file):
        df = pd.read_csv(file)
        df_new = pd.DataFrame()

        tz = dt.timezone(dt.timedelta(hours = 7))
        date = dt.datetime.now(tz=tz).strftime("%Y-%m-%d %H:%M:%S")

        df_new['stream_code'] = df['channelCode'].str.upper()
        df_new['start_date'] = df['startAt']
        df_new['end_date'] = df['endAt']
        df_new['title_name'] = df['titleName']
        df_new['maintain_by'] = "MANUAL"
        df_new['status'] = "QUEUE"
        df_new['created_date'] = date
        df_new['created_by'] = "ANONYMUS"
        return df_new
        # print(df['channelCode'].str.upper(), df['startAt'], df['endAt'], df['titleName'], "MANUAL", "QUEUE")

def InsertQuery(code, start, end, title, maintain, status ,createData, createBy):
    connection = cx_Oracle.connect(
        user="obmsapp",
        password="obms$user_01",
        dsn="10.18.87.36:1522/obmsdb"
    )
    cur = connection.cursor()
    cur.execute("insert into STM_BLACKOUT (stream_code, start_date, end_date, title_name, maintain_by, status, created_date, created_by) values ('{}','{}','{}')".format(code, start, end, title, maintain, status ,createData, createBy))
    connection.commit()
    connection.close()

def selectQuery(code, start, end, title, status):
    connection = cx_Oracle.connect(
        user="obmsapp",
        password="obms$user_01",
        dsn="10.18.87.36:1522/obmsdb",
        encoding="UTF-8", nencoding="UTF-8"
    )
    cur = connection.cursor()

    query = "select stream_code, start_date, end_date, title_name, status " \
    + "from STM_BLACKOUT " \
    + "where stream_code = '{}' ".format(code) \
    + """and start_date = to_timestamp('{}', 'YYYY-MM-DD"T"HH24:MI:SS.ff3"Z"') """.format(str(start)) \
    + """and end_date = to_timestamp('{}', 'YYYY-MM-DD"T"HH24:MI:SS.ff3"Z"') """.format(str(end)) \
    + "and title_name like '%{}%' ".format(title) \
    + "and status = '{}' ".format(status)

    cur.execute(query)
    row = cur.fetchone()
    connection.close()
    return row

while True:
    for file in searchFileName():

        print(os.getenv('urls_production'))
        conn = http.client.HTTPConnection(os.getenv('urls_production'))
        dataList = []
        boundary = 'wL36Yn8afVp8Ag7AmP8qZ0SA4n1v9T'
        dataList.append(encode('--' + boundary))
        dataList.append(encode('Content-Disposition: form-data; name=file; filename={0}'.format('ch8-31102021.csv')))

        fileType = mimetypes.guess_type(file)[0] or 'application/octet-stream'
        dataList.append(encode('Content-Type: {}'.format(fileType)))
        dataList.append(encode(''))

        with open(file, 'rb') as f:
            dataList.append(f.read())
        dataList.append(encode('--'+boundary+'--'))
        dataList.append(encode(''))
        body = b'\r\n'.join(dataList)
        payload = body
        headers = {
        'Content-type': 'multipart/form-data; boundary={}'.format(boundary)
        }
        conn.request("POST", "/api/v1/LiveChannel/blackout/import", payload, headers)
        res = conn.getresponse()
        data = res.read()
        print(data.decode("utf-8"))
    time.sleep(6000)
