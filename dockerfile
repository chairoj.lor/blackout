# FROM jupyter/all-spark-notebook
FROM python:latest

USER root
RUN apt-get update -y
RUN apt-get install -y build-essential unzip libaio-dev npm
RUN pip install pandas cx_oracle pymongo

RUN ldconfig

RUN mkdir /opt/app
RUN mkdir /opt/app/data
WORKDIR /opt/app
RUN wget --no-check-certificate https://download.oracle.com/otn_software/linux/instantclient/211000/instantclient-basic-linux.x64-21.1.0.0.0.zip
RUN wget --no-check-certificate https://download.oracle.com/otn_software/linux/instantclient/211000/instantclient-sdk-linux.x64-21.1.0.0.0.zip
RUN unzip instantclient-basic-linux* 
RUN unzip instantclient-sdk-linux*

COPY main.py ./main.py
ENV LD_LIBRARY_PATH=/opt/app/instantclient_21_1
